﻿1
00:00:00,100 --> 00:00:03,360
 Mary hija sid ta 'ħanut tal-kafè u ddeċidiet li trid taċċettaha

2
00:00:03,360 --> 00:00:05,875
 Pagamenti fi flus kontanti Bitcoin biex jattiraw klijenti ġodda.

3
00:00:05,880 --> 00:00:11,200
 Hija niżżlet il-kartiera Bitcoin.com u bdiet taċċetta Bitcoin Cash minnufih.

4
00:00:11,360 --> 00:00:14,180
 "Marija normalment tkun fil-ħanut, imma xi kultant

5
00:00:14,180 --> 00:00:17,475
 hi teħtieġ li titwarrab u titmexxa minn żewġ assistenti tagħha,

6
00:00:17,475 --> 00:00:18,685
 Peter u Sarah.

7
00:00:19,060 --> 00:00:21,620
 Marija riedet triq faċli għal Pietru u Sarah

8
00:00:21,620 --> 00:00:25,120
 biex tkompli taċċetta ħlasijiet Bitcoin Cash waqt li kienet 'il bogħod mill-ħanut.

9
00:00:25,615 --> 00:00:27,975
 Daħħal l-App Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
 Mary tista 'tinstalla l-App Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
 direttament fuq it-telefon ta 'Peter u Sarah, jew fuq il-pillola tal-maħżen,

12
00:00:34,620 --> 00:00:37,560
 u jkollhom il-ħlasijiet lill-maħżen mibgħuta direttament lill-kartiera tagħha stess.

13
00:00:37,720 --> 00:00:40,480
 Issa Marija għandha s-serħan tal-moħħ li hi tista ’taċċetta

14
00:00:40,480 --> 00:00:43,740
 Ħlasijiet ta 'Bitcoin Cash veloċi, irħas u affidabbli,

15
00:00:43,740 --> 00:00:45,740
 anke meta ma tkunx fil-ħanut.

16
00:00:45,740 --> 00:00:47,040
 Allura kif għamlet Marija?

17
00:00:47,575 --> 00:00:50,675
 Biex tibda, sempliċement mur l-App Store jew il-maħżen Play

18
00:00:50,680 --> 00:00:52,860
 u niżżel l-App Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
 Issa, ejja nibdew inwaqqfu.

20
00:00:55,880 --> 00:00:59,980
 Wara li tissettja l-kodiċi tal-pin tiegħek, ibda billi ddaħħal l-isem tal-maħżen tiegħek.

21
00:01:00,500 --> 00:01:04,600
 Ikklikkja l-Indirizz tad-Destinazzjoni u Skennja jew Ippejstja l-indirizz li tirċievi fi flus kontanti Bitcoin

22
00:01:05,760 --> 00:01:09,460
 Jekk m'għandekx kartiera ta 'Bitcoin Cash, tista' tniżżel il-Kartiera Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
 għal apparati IOS jew Android.

24
00:01:12,980 --> 00:01:15,440
 Fl-aħħarnett, agħżel il-Munita Lokali tiegħek.

25
00:01:16,300 --> 00:01:17,300
 Dak hu!

26
00:01:17,560 --> 00:01:18,840
 Int imiss kollox.

27
00:01:19,165 --> 00:01:21,555
 Daħħal l-ammont biex tiġġenera kodiċi QR.

28
00:01:22,075 --> 00:01:25,025
 Il-klijenti tiegħek biss għandhom bżonn jiskennjaw u jikkonfermaw il-pagament

29
00:01:25,025 --> 00:01:27,115
 u se tintbagħat direttament fil-kartiera tiegħek.

30
00:01:28,040 --> 00:01:30,480
 Peress li l-app hija mħarsa minn pin-code,

31
00:01:30,480 --> 00:01:33,220
 Marija biss tista ’tiddeċiedi fejn jintbagħtu l-fondi.

32
00:01:33,795 --> 00:01:36,945
 Sempliċi, Sikuri, u aktar malajr minn Beraq.

33
00:01:37,755 --> 00:01:39,700
 Reġistru tal-Cash Bitcoin

