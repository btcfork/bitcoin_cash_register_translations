﻿1
00:00:00,100 --> 00:00:03,360
 Marija ir kafejnīcas īpašniece un nolēma, ka vēlas to pieņemt

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash maksājumi jaunu klientu piesaistīšanai.

3
00:00:05,880 --> 00:00:11,200
 Viņa lejupielādēja Bitcoin.com maku un uzreiz sāka pieņemt Bitcoin Cash.

4
00:00:11,360 --> 00:00:14,180
 "Marija parasti ir veikalā, bet dažreiz

5
00:00:14,180 --> 00:00:17,475
 viņai vajag atkāpties un panākt, lai to vada abi viņas palīgi,

6
00:00:17,475 --> 00:00:18,685
 Pēteris un Sāra.

7
00:00:19,060 --> 00:00:21,620
 Marija gribēja vieglu ceļu Pēterim un Sārai

8
00:00:21,620 --> 00:00:25,120
 turpināt pieņemt Bitcoin Cash maksājumus, kamēr viņa bija prom no veikala.

9
00:00:25,615 --> 00:00:27,975
 Ievadiet lietotni Bitcoin kases aparāts.

10
00:00:28,415 --> 00:00:31,495
 Marija var instalēt Bitcoin kases aparāta lietotni

11
00:00:31,500 --> 00:00:34,620
 tieši Pētera un Sāras tālrunī vai veikala planšetdatorā,

12
00:00:34,620 --> 00:00:37,560
 un maksājumus uz veikalu sūta tieši uz viņas maku.

13
00:00:37,720 --> 00:00:40,480
 Tagad Marijai ir miers, ko viņa var pieņemt

14
00:00:40,480 --> 00:00:43,740
 ātri, lēti un uzticami Bitcoin Cash maksājumi,

15
00:00:43,740 --> 00:00:45,740
 pat tad, kad viņa nav veikalā.

16
00:00:45,740 --> 00:00:47,040
 Tātad, kā Marija to izdarīja?

17
00:00:47,575 --> 00:00:50,675
 Lai sāktu, vienkārši dodieties uz App Store vai Play veikalu

18
00:00:50,680 --> 00:00:52,860
 un lejupielādējiet lietotni Bitcoin kases aparāts.

19
00:00:53,720 --> 00:00:55,320
 Tagad pieņemsim jūs iestatīt.

20
00:00:55,880 --> 00:00:59,980
 Pēc PIN koda iestatīšanas vispirms ievadiet sava veikala nosaukumu.

21
00:01:00,500 --> 00:01:04,600
 Noklikšķiniet uz Galamērķa adrese un skenējiet vai ielīmējiet savu Bitcoin naudas saņemšanas adresi

22
00:01:05,760 --> 00:01:09,460
 Ja jums nav Bitcoin Cash seifa, varat lejupielādēt Bitcoin.com maku

23
00:01:09,460 --> 00:01:12,120
 iOS vai Android ierīcēm.

24
00:01:12,980 --> 00:01:15,440
 Visbeidzot, atlasiet vietējo valūtu.

25
00:01:16,300 --> 00:01:17,300
 Tieši tā!

26
00:01:17,560 --> 00:01:18,840
 Jūs visi esat gatavs iet.

27
00:01:19,165 --> 00:01:21,555
 Ievadiet summu, lai ģenerētu QR kodu.

28
00:01:22,075 --> 00:01:25,025
 Jūsu klientiem vienkārši ir jāpārbauda un jāapstiprina maksājums

29
00:01:25,025 --> 00:01:27,115
 un tas tiks nosūtīts tieši uz jūsu maku.

30
00:01:28,040 --> 00:01:30,480
 Tā kā lietotne ir aizsargāta ar PIN kodu,

31
00:01:30,480 --> 00:01:33,220
 tikai Marija var izlemt, kur tiek nosūtīti līdzekļi.

32
00:01:33,795 --> 00:01:36,945
 Vienkāršs, drošs un ātrāks nekā zibens.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin kases aparāts

