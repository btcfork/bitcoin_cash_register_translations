﻿1
00:00:00,100 --> 00:00:03,360
 Mae Mary yn berchennog siop goffi a phenderfynodd ei bod am dderbyn

2
00:00:03,360 --> 00:00:05,875
 Taliadau Bitcoin Cash i ddenu cwsmeriaid newydd.

3
00:00:05,880 --> 00:00:11,200
 Fe wnaeth hi lawrlwytho waled Bitcoin.com a dechrau derbyn Bitcoin Cash ar unwaith.

4
00:00:11,360 --> 00:00:14,180
 "Mae Mary fel arfer yn y siop, ond weithiau

5
00:00:14,180 --> 00:00:17,475
 mae angen iddi gamu i ffwrdd a chael ei rhedeg gan ei dau gynorthwyydd,

6
00:00:17,475 --> 00:00:18,685
 Peter a Sarah.

7
00:00:19,060 --> 00:00:21,620
 Roedd Mary eisiau ffordd hawdd i Peter a Sarah

8
00:00:21,620 --> 00:00:25,120
 i barhau i dderbyn taliadau Bitcoin Cash tra roedd hi i ffwrdd o'r siop.

9
00:00:25,615 --> 00:00:27,975
 Rhowch yr App Cofrestr Arian Parod Bitcoin.

10
00:00:28,415 --> 00:00:31,495
 Gall Mary osod yr App Cofrestr Arian Parod Bitcoin

11
00:00:31,500 --> 00:00:34,620
 yn uniongyrchol ar ffôn Peter a Sarah, neu ar dabled y siop,

12
00:00:34,620 --> 00:00:37,560
 ac anfon taliadau i'r siop yn uniongyrchol i'w waled ei hun.

13
00:00:37,720 --> 00:00:40,480
 Nawr mae gan Mair dawelwch meddwl y gall ei dderbyn

14
00:00:40,480 --> 00:00:43,740
 taliadau Bitcoin Cash cyflym, rhad a dibynadwy,

15
00:00:43,740 --> 00:00:45,740
 hyd yn oed pan nad yw hi yn y siop.

16
00:00:45,740 --> 00:00:47,040
 Felly sut wnaeth Mary hynny?

17
00:00:47,575 --> 00:00:50,675
 I ddechrau, ewch i'r App Store neu'r siop Chwarae

18
00:00:50,680 --> 00:00:52,860
 a dadlwythwch Ap Cofrestr Arian Parod Bitcoin.

19
00:00:53,720 --> 00:00:55,320
 Nawr, gadewch i ni eich sefydlu.

20
00:00:55,880 --> 00:00:59,980
 Ar ôl gosod eich cod pin, dechreuwch trwy fewnbynnu enw eich siop.

21
00:01:00,500 --> 00:01:04,600
 Cliciwch Cyfeiriad Cyrchfan a Sganiwch neu Gludwch eich cyfeiriad Derbyn Arian Parod Bitcoin

22
00:01:05,760 --> 00:01:09,460
 Os nad oes gennych waled Bitcoin Cash, gallwch lawrlwytho Waled Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
 ar gyfer dyfeisiau iOS neu Android.

24
00:01:12,980 --> 00:01:15,440
 Yn olaf, dewiswch eich Arian Cyfred Lleol.

25
00:01:16,300 --> 00:01:17,300
 Dyna ni!

26
00:01:17,560 --> 00:01:18,840
 Rydych chi i gyd ar fin mynd.

27
00:01:19,165 --> 00:01:21,555
 Rhowch y swm i gynhyrchu cod QR.

28
00:01:22,075 --> 00:01:25,025
 Mae angen i'ch cwsmeriaid sganio a chadarnhau'r taliad yn unig

29
00:01:25,025 --> 00:01:27,115
 a bydd yn cael ei anfon yn uniongyrchol i'ch waled.

30
00:01:28,040 --> 00:01:30,480
 Gan fod yr ap wedi'i warchod gan god pin,

31
00:01:30,480 --> 00:01:33,220
 dim ond Mary all benderfynu ble mae'r arian yn cael ei anfon.

32
00:01:33,795 --> 00:01:36,945
 Syml, Diogel, a Chyflymach na Mellt.

33
00:01:37,755 --> 00:01:39,700
 Cofrestr Arian Parod Bitcoin

