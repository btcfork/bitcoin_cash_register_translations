﻿1
00:00:00,100 --> 00:00:03,360
Mary egy kávézó tulajdonosa, és úgy döntött, hogy mostantól elfogad

2
00:00:03,360 --> 00:00:05,875
Bitcoin Cash-t is, hogy új ügyfeleket vonzzon üzletébe.

3
00:00:05,880 --> 00:00:11,200
Letöltötte a Bitcoin.com pénztárca alkalmazását, és elkezdett a Bitcoin Cash-t elfogadni.

4
00:00:11,360 --> 00:00:14,180
Mary általában a boltban van, de néha

5
00:00:14,180 --> 00:00:17,475
ki kell ugrania, és ilyenkor
két asszisztensére bízza az üzletet,

6
00:00:17,475 --> 00:00:18,685
Peter-re és Sarah-ra.

7
00:00:19,060 --> 00:00:21,620
Mary szerette volna, hogy Peter és Sarah

8
00:00:21,620 --> 00:00:25,120
akkor is el tudjon fogadni Bitcoin Cash-t amikor ő távol van.

9
00:00:25,480 --> 00:00:28,260
Erre találtuk ki a Bitcoin Cash Register pénztárgép alkalmazást.

10
00:00:28,420 --> 00:00:31,500
Mary telepítheti a Bitcoin Cash pénztárgép alkalmazást

11
00:00:31,500 --> 00:00:34,840
közvetlenül Peter és Sarah telefonjára vagy az üzletben lévő táblagépre,

12
00:00:34,840 --> 00:00:37,660
és az alkalmazás a vásárlások értékét saját pénztárcájába küldi el.

13
00:00:37,720 --> 00:00:40,480
Így Mary megnyugodhat, hogy

14
00:00:40,480 --> 00:00:43,740
a gyors, olcsó és megbízható Bitcoin Cash-t

15
00:00:43,740 --> 00:00:45,740
akkor is el tudja fogadni, ha nincs éppen a boltban.

16
00:00:45,740 --> 00:00:47,040
Hogyan csinálta ezt Mary?

17
00:00:47,575 --> 00:00:50,675
Először is lépj be az App Store vagy a Google Play áruházba

18
00:00:50,680 --> 00:00:52,860
és töltsd le a Bitcoin Cash Register pénztárgép alkalmazást.

19
00:00:53,720 --> 00:00:55,320
Ha letöltötted, beszéljünk a beállításokról.

20
00:00:55,880 --> 00:00:59,980
A PIN-kód megadása után kezdd azzal, hogy megadod a bolt nevét.

21
00:01:00,500 --> 00:01:05,620
Kattints a Cél cím megadása elemre, és olvasd be vagy másold be a Bitcoin Cash címet amire fogadni szeretnéd a pénzt.

22
00:01:05,760 --> 00:01:09,800
Ha még nincs Bitcoin Cash-pénztárcád, töltsd le a Bitcoin.com Wallet pénztárca alkalmazást

23
00:01:09,800 --> 00:01:12,120
iOS vagy Android eszközre.

24
00:01:12,980 --> 00:01:15,440
Végül válaszd ki a helyi pénznemet.

25
00:01:16,300 --> 00:01:17,300
Ennyi!

26
00:01:17,560 --> 00:01:18,840
Készen is vagy.

27
00:01:19,165 --> 00:01:21,555
Írd be a fizetendő összeget és megjelenik egy QR-kód.

28
00:01:22,075 --> 00:01:25,025
Az ügyfeleknek csak be kell scannelniük és leokézni a telefonjukon a fizetést

29
00:01:25,025 --> 00:01:27,115
és közvetlenül a pénztárcádban landol az összeg.

30
00:01:28,040 --> 00:01:30,480
Mivel az alkalmazást PIN-kód védi,

31
00:01:30,480 --> 00:01:33,220
csak Mary tudja eldönteni, hogy hová küldi a bevételt.

32
00:01:33,795 --> 00:01:36,945
Egyszerű, biztonságos és gyorsabb, mint a villám.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register

