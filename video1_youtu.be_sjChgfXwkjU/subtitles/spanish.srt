﻿1
00:00:00,100 --> 00:00:03,360
Mary es la dueña de una cafeteria y decidio
que desea aceptar

2
00:00:03,360 --> 00:00:05,875
Pagos en Bitcoin Cash para atraer nuevos clientes.

3
00:00:05,880 --> 00:00:11,200
Ella descargo la billetera Bitcoin.com y ha comenzado a aceptar Bitcoin Cash immediatemente.

4
00:00:11,360 --> 00:00:14,180
Mary usualmente esta en la tienda, pero a veces

5
00:00:14,180 --> 00:00:17,475
ella necesita salir y
dejar a cargo a sus dos asistentes,

6
00:00:17,475 --> 00:00:18,685
Peter y Sarah.

7
00:00:19,060 --> 00:00:21,620
Mary desea una forma facil para que Peter y Sarah

8
00:00:21,620 --> 00:00:25,120
continuen aceptando pagos en Bitcoin Cash mientras ella esta lejos de la tienda.

9
00:00:25,615 --> 00:00:27,975
Entra a la aplicacion Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
Mary puede instalar la aplicacion Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
directamente en los telefonos de Peter y Sarah, o en la tablet de la tienda,

12
00:00:34,620 --> 00:00:37,560
y los pagos que se realicen en la tienda seran enviados directamente a su propia billetera.

13
00:00:37,720 --> 00:00:40,480
Ahora Mary puede estar tranquila de aceptar

14
00:00:40,480 --> 00:00:43,740
pagos en Bitcoin Cash rapidos, baratos y confiables,

15
00:00:43,740 --> 00:00:45,740
incluso sin estar en la tienda.

16
00:00:45,740 --> 00:00:47,040
¿Como Mary lo hace?

17
00:00:47,575 --> 00:00:50,675
para iniciar, simplemente busca en la tienda de aplicaciones App Store o Play store

18
00:00:50,680 --> 00:00:52,860
y descarga la aplicacion Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
Ahora, Vamos a configurarla.

20
00:00:55,880 --> 00:00:59,980
Despues de establecer un codigo PIN, comience por ingresar el nombre de tu tienda.

21
00:01:00,500 --> 00:01:04,600
Click en direccion de destino y Escanea o Pega tu direccion de recepcion Bitcoin Cash

22
00:01:05,760 --> 00:01:09,460
En caso de que no poseas una billetera Bitcoin Cash, puedes descargar la billetera Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
para iOS o para dispositivos Android.

24
00:01:12,980 --> 00:01:15,440
por ultimo, selecciona tu moneda local.

25
00:01:16,300 --> 00:01:17,300
Es todo!

26
00:01:17,560 --> 00:01:18,840
Estas listo para comenzar.

27
00:01:19,165 --> 00:01:21,555
Ingresa el monto para generar un codigo QR.

28
00:01:22,075 --> 00:01:25,025
tus clientes solo requieren escanear y confirmar el pago

29
00:01:25,025 --> 00:01:27,115
y sera enviado directamente a tu billetera.

30
00:01:28,040 --> 00:01:30,480
Desde que la aplicacion esta protegida con un codigo PIN,

31
00:01:30,480 --> 00:01:33,220
Unicamente Mary puede decidir a donde seran enviados los fondos.

32
00:01:33,795 --> 00:01:36,945
Simple, Seguro, y Rapido como una Rayo.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register
