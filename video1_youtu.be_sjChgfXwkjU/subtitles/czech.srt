﻿1
00:00:00,100 --> 00:00:03,360
Marie vlastní kavárnu a rozhodla se příjmat platby

2
00:00:03,360 --> 00:00:05,875
v Bitcoin Cash aby přilákala nové zákazníky.

3
00:00:05,880 --> 00:00:11,200
Stáhla si peněženku Bitcoin.com Wallet a ihned začala příjímat Bicoin Cash.

4
00:00:11,360 --> 00:00:14,180
Marie je většinou v kavárně, ale občas

5
00:00:14,180 --> 00:00:17,475
musí chod kavárny přenechat
svým dvoum asistentům,

6
00:00:17,475 --> 00:00:18,685
Petrovi a Sáře.

7
00:00:19,060 --> 00:00:21,620
Marie chtěla jednoduchý způsob aby

8
00:00:21,620 --> 00:00:25,120
Petr a Sára mohli příjímat Bitcoin Cash když ona nebude v kavárně.

9
00:00:25,615 --> 00:00:27,975
Představujeme aplikaci Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
Marie může nainstalovat aplikaci Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
přímo do mobilů Petra a Sáry, nebo do tabletu v kavárně

12
00:00:34,620 --> 00:00:37,560
a mít platby v kavárně poslané do její peněženky.

13
00:00:37,720 --> 00:00:40,480
Nyní může v klidu přijímat

14
00:00:40,480 --> 00:00:43,740
rychlé, levné a spolehlivé platby Bitcoin Cash,

15
00:00:43,740 --> 00:00:45,740
i když zrovna není v kavárně.

16
00:00:45,740 --> 00:00:47,040
Jak to tedy Marie udělala?

17
00:00:47,575 --> 00:00:50,675
Jednoduše jděte do App Store nebo Play Store

18
00:00:50,680 --> 00:00:52,860
a stáhněte si aplikaci Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
Nyní pojďme začít.

20
00:00:55,880 --> 00:00:59,980
Potom co si nastavíte pin kód, zadejte jméno Vašeho obchodu.

21
00:01:00,500 --> 00:01:04,600
Klikněte na cílovou adresu a naskenujte nebo
vložte Vaší příjimací Bitcoin Cash adresu

22
00:01:05,760 --> 00:01:09,460
Pokud nemáte Bitcoin Cash peněženku, můžete si stáhnout Bitcoin.com Wallet

23
00:01:09,460 --> 00:01:12,120
pro iOS nebo Android.

24
00:01:12,980 --> 00:01:15,440
Nakonec vyberte Vaši místní měnu.

25
00:01:16,300 --> 00:01:17,300
To je vše!

26
00:01:17,560 --> 00:01:18,840
Všechno je připraveno.

27
00:01:19,165 --> 00:01:21,555
Zadejte částku pro vygenerování QR kódu.

28
00:01:22,075 --> 00:01:25,025
Vaši zákazníci musí jen kód naskenovat a potvrdit platbu

29
00:01:25,025 --> 00:01:27,115
která se pošle rovnou na Vaši peněženku.

30
00:01:28,040 --> 00:01:30,480
Protože aplikace je chráněna pinem,

31
00:01:30,480 --> 00:01:33,220
pouze Marie může určit kam pošle peníze.

32
00:01:33,795 --> 00:01:36,945
Jednoduché, Bezpečné a Rychlejší než Blesk.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register
