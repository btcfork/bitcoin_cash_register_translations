﻿1
00:00:00,100 --> 00:00:03,360
Mary é dona de uma cafeteria e decidiu
Que deseja aceitar

2
00:00:03,360 --> 00:00:05,875
Pagamentos em Bitcoin Cash para atrair novos clientes.

3
00:00:05,880 --> 00:00:11,200
Ela baixou a carteira Bitcoin.com e começou a aceitar o Bitcoin Cash dessa maneira.

4
00:00:11,360 --> 00:00:14,180
Mary geralmente está na loja, mas às vezes

5
00:00:14,180 --> 00:00:17,475
ela precisa sair e
deixar no comando seus dois assistentes,

6
00:00:17,475 --> 00:00:18,685
Peter e Sarah.

7
00:00:19,060 --> 00:00:21,620
Mary quer uma maneira fácil para que Peter e Sarah

8
00:00:21,620 --> 00:00:25,120
Continuem a aceitar pagamentos em Bitcoin Cash enquanto ela está longe da loja

9
00:00:25,615 --> 00:00:27,975
Eis o aplicativo Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
Mary pode instalar o aplicativo Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
diretamente nos telefones de Peter e Sarah, ou na tablet da loja,

12
00:00:34,620 --> 00:00:37,560
e os pagamentos feitos na loja serão enviados diretamente para sua própria carteira.

13
00:00:37,720 --> 00:00:40,480
Agora Mary está tranquila e pode aceitar

14
00:00:40,480 --> 00:00:43,740
Pagamentos em Bitcoin Cash rápidos, baratos e confiáveis,

15
00:00:43,740 --> 00:00:45,740
Mesmo sem estar na loja.

16
00:00:45,740 --> 00:00:47,040
Como Mary faz isso?

17
00:00:47,575 --> 00:00:50,675
Para começar, basta pesquisar na loja de aplicativos

18
00:00:50,680 --> 00:00:52,860
e baixe o aplicativo Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
Agora, vamos configurá-lo.

20
00:00:55,880 --> 00:00:59,980
Depois de estabelecer um código PIN, comece digitando o nome da sua loja.

21
00:01:00,500 --> 00:01:04,600
Clique no endereço de destino e digitalize ou cole seu endereço de recepção do Bitcoin Cash

22
00:01:05,760 --> 00:01:09,460
Caso você não tenha uma carteira Bitcoin Cash, você pode fazer o download da carteira Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
para dispositivos iOS ou Android.

24
00:01:12,980 --> 00:01:15,440
Por ultimo, selecione sua moeda local.

25
00:01:16,300 --> 00:01:17,300
É tudo!

26
00:01:17,560 --> 00:01:18,840
Você está pronto para começar.

27
00:01:19,165 --> 00:01:21,555
Digite o valor para gerar um código QR.

28
00:01:22,075 --> 00:01:25,025
Seus clientes vão apenas digitalizar e confirmar o pagamento

29
00:01:25,025 --> 00:01:27,115
e será enviado diretamente para sua carteira.

30
00:01:28,040 --> 00:01:30,480
Uma vez que o aplicativo está protegido com um código PIN,

31
00:01:30,480 --> 00:01:33,220
Somente Mary pode decidir para onde os fundos serão enviados.

32
00:01:33,795 --> 00:01:36,945
Simples, seguro e rápido como um raio.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register
