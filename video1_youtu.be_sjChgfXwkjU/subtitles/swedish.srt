﻿1
00:00:00,100 --> 00:00:03,360
 Mary är kaféägare och beslutade att hon vill acceptera

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash-betalningar för att locka nya kunder.

3
00:00:05,880 --> 00:00:11,200
 Hon laddade ned Bitcoin.com-plånboken och började acceptera Bitcoin Cash direkt.

4
00:00:11,360 --> 00:00:14,180
 Mary är vanligtvis i butiken, men ibland

5
00:00:14,180 --> 00:00:17,475
 hon måste gå bort och få det drivet av sina två assistenter,

6
00:00:17,475 --> 00:00:18,685
 Peter och Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary ville ha ett enkelt sätt för Peter och Sarah

8
00:00:21,620 --> 00:00:25,120
 att fortsätta acceptera Bitcoin Cash-betalningar medan hon var borta från butiken.

9
00:00:25,615 --> 00:00:27,975
 Ange Bitcoin Cash Register-appen.

10
00:00:28,415 --> 00:00:31,495
 Mary kan installera Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
 direkt på Peter och Sarahs telefon eller på butikstabletten,

12
00:00:34,620 --> 00:00:37,560
 och betalningar till butiken skickas direkt till hennes egen plånbok.

13
00:00:37,720 --> 00:00:40,480
 Nu har Mary sinnesfrid som hon kan acceptera

14
00:00:40,480 --> 00:00:43,740
 snabba, billiga och tillförlitliga Bitcoin Cash-betalningar,

15
00:00:43,740 --> 00:00:45,740
 även när hon inte är i butiken.

16
00:00:45,740 --> 00:00:47,040
 Så hur gjorde Mary det?

17
00:00:47,575 --> 00:00:50,675
 För att komma igång går du helt enkelt till App Store eller Play store

18
00:00:50,680 --> 00:00:52,860
 och ladda ner Bitcoin Cash Register App.

19
00:00:53,720 --> 00:00:55,320
 Låt oss nu sätta upp dig.

20
00:00:55,880 --> 00:00:59,980
 När du har ställt in din pinkod börjar du med att skriva in butikens namn.

21
00:01:00,500 --> 00:01:04,600
 Klicka på Destinationsadress och skanna eller klistra in din Bitcoin Cash-mottagningsadress

22
00:01:05,760 --> 00:01:09,460
 Om du inte har en Bitcoin Cash-plånbok kan du ladda ner Bitcoin.com-plånboken

23
00:01:09,460 --> 00:01:12,120
 för iOS- eller Android-enheter.

24
00:01:12,980 --> 00:01:15,440
 Välj slutligen din lokala valuta.

25
00:01:16,300 --> 00:01:17,300
 Det är allt!

26
00:01:17,560 --> 00:01:18,840
 Du är redo att gå.

27
00:01:19,165 --> 00:01:21,555
 Ange beloppet för att generera en QR-kod.

28
00:01:22,075 --> 00:01:25,025
 Dina kunder behöver bara skanna och bekräfta betalningen

29
00:01:25,025 --> 00:01:27,115
 och det skickas direkt till din plånbok.

30
00:01:28,040 --> 00:01:30,480
 Eftersom appen är skyddad med en pinkod,

31
00:01:30,480 --> 00:01:33,220
 bara Mary kan bestämma vart medlen skickas.

32
00:01:33,795 --> 00:01:36,945
 Enkelt, säkert och snabbare än blixt.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin kassaapparat

