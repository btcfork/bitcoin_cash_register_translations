﻿1
00:00:00,100 --> 00:00:03,360
 Mary on kohviku omanik ja otsustas, et soovib temaga nõustuda

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash maksed uute klientide ligimeelitamiseks.

3
00:00:05,880 --> 00:00:11,200
 Ta laadis alla Bitcoin.com rahakoti ja asus kohe Bitcoin Cash aktsepteerima.

4
00:00:11,360 --> 00:00:14,180
 "Maarja on tavaliselt poes, aga vahel

5
00:00:14,180 --> 00:00:17,475
 ta peab eemalduma ja laskma sellel oma kahe abilise hallata,

6
00:00:17,475 --> 00:00:18,685
 Peeter ja Saara.

7
00:00:19,060 --> 00:00:21,620
 Maarja soovis Peetrusele ja Saarale lihtsat viisi

8
00:00:21,620 --> 00:00:25,120
 jätkata Bitcoin Cash maksete aktsepteerimist, kui ta oli poest eemal.

9
00:00:25,615 --> 00:00:27,975
 Sisestage Bitcoini kassaaparaadi rakendus.

10
00:00:28,415 --> 00:00:31,495
 Mary saab installida Bitcoini kassaaparaadi rakenduse

11
00:00:31,500 --> 00:00:34,620
 otse Peetri ja Saara telefonis või poe tahvelarvutis,

12
00:00:34,620 --> 00:00:37,560
 ja lasta kauplusesse tehtavad maksed otse tema enda rahakotile.

13
00:00:37,720 --> 00:00:40,480
 Nüüd on Maarjal meelerahu, mida ta saab aktsepteerida

14
00:00:40,480 --> 00:00:43,740
 kiire, odav ja usaldusväärne Bitcoin Cash makse,

15
00:00:43,740 --> 00:00:45,740
 isegi siis, kui teda poes pole.

16
00:00:45,740 --> 00:00:47,040
 Kuidas Mary seda tegi?

17
00:00:47,575 --> 00:00:50,675
 Alustamiseks minge lihtsalt App Store'i või Play poodi

18
00:00:50,680 --> 00:00:52,860
 ja laadige alla Bitcoini kassaaparaadi rakendus.

19
00:00:53,720 --> 00:00:55,320
 Nüüd laseme teil end paika panna.

20
00:00:55,880 --> 00:00:59,980
 Pärast PIN-koodi määramist sisestage kõigepealt oma poe nimi.

21
00:01:00,500 --> 00:01:04,600
 Klõpsake valikul Sihtkoha aadress ja skannige või kleepige oma Bitcoini raha vastuvõtmise aadress

22
00:01:05,760 --> 00:01:09,460
 Kui teil pole Bitcoin Cash-i rahakotti, saate alla laadida Bitcoin.com-i rahakoti

23
00:01:09,460 --> 00:01:12,120
 iOS- või Android-seadmetele.

24
00:01:12,980 --> 00:01:15,440
 Lõpuks valige oma kohalik valuuta.

25
00:01:16,300 --> 00:01:17,300
 Ongi!

26
00:01:17,560 --> 00:01:18,840
 Olete kõik valmis minema.

27
00:01:19,165 --> 00:01:21,555
 Sisestage summa QR-koodi genereerimiseks.

28
00:01:22,075 --> 00:01:25,025
 Teie kliendid peavad lihtsalt makse skaneerima ja kinnitama

29
00:01:25,025 --> 00:01:27,115
 ja see saadetakse otse teie rahakotile.

30
00:01:28,040 --> 00:01:30,480
 Kuna rakendus on kaitstud PIN-koodiga,

31
00:01:30,480 --> 00:01:33,220
 Ainult Maarja saab otsustada, kuhu raha saadetakse.

32
00:01:33,795 --> 00:01:36,945
 Lihtne, turvaline ja kiirem kui välk.

33
00:01:37,755 --> 00:01:39,700
 Bitcoini kassaaparaat

