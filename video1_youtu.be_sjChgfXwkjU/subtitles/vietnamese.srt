﻿1
00:00:00,100 --> 00:00:03,360
Mary là chủ một quán cà phê và quyết định rằng
cô muốn chấp nhận

2
00:00:03,360 --> 00:00:05,875
Bitcoin Cash thanh toán để thu hút khách hàng mới.

3
00:00:05,880 --> 00:00:11,200
Cô tải về ví Bitcoin.com và đã bắt đầu chấp nhận Bitcoin Cash ngay lập tức.

4
00:00:11,360 --> 00:00:14,180
Mary thường có mặt tại các cửa hàng, nhưng đôi khi

5
00:00:14,180 --> 00:00:17,475
cô ấy có việc phải vắng mặt và
Việc vận hành nhờ hai trợ lý của cô,

6
00:00:17,475 --> 00:00:18,685
Peter và Sarah.

7
00:00:19,060 --> 00:00:21,620
Mary muốn thật  dễ dàng cho Peter và Sarah

8
00:00:21,620 --> 00:00:25,120
để tiếp tục chấp nhận thanh toán Bitcoin Cash trong khi cô đã đi từ cửa hàng.

9
00:00:25,615 --> 00:00:27,975
Nhập ứng dụng Bitcoin Cash đăng ký.

10
00:00:28,415 --> 00:00:31,495
Mary có thể cài đặt các Bitcoin Cash đăng ký ứng dụng

11
00:00:31,500 --> 00:00:34,620
trực tiếp trên điện thoại của Peter và Sarah, hoặc trên máy tính bảng cửa hàng,

12
00:00:34,620 --> 00:00:37,560
và có các khoản thanh toán cho cửa hàng được gửi trực tiếp đến ví của riêng mình.

13
00:00:37,720 --> 00:00:40,480
Bây giờ Mary đã yên tâm rằng cô có thể chấp nhận

14
00:00:40,480 --> 00:00:43,740
nhanh chóng, giá rẻ và đáng tin cậy Bitcoin Cash thanh toán,

15
00:00:43,740 --> 00:00:45,740
ngay cả khi cô ấy không phải ở cửa hàng.

16
00:00:45,740 --> 00:00:47,040
Vậy làm thế nào Mary làm điều đó?

17
00:00:47,575 --> 00:00:50,675
Để bắt đầu, chỉ cần truy cập ngay  App Store hoặc ứng dụng Google Play

18
00:00:50,680 --> 00:00:52,860
và tải về các ứng dụng dùng để đăng ký Bitcoin Cash.

19
00:00:53,720 --> 00:00:55,320
Bây giờ, chúng ta hãy thiết lập.

20
00:00:55,880 --> 00:00:59,980
Sau khi thiết lập mã pin của bạn, bắt đầu bằng cách nhập vào tên của cửa hàng của bạn.

21
00:01:00,500 --> 00:01:04,600
Nhấp vào địa chỉ đích và quét hoặc dán địa chỉ nhận tiền mặt Bitcoin của bạn

22
00:01:05,760 --> 00:01:09,460
Nếu bạn không có ví Bitcoin Cash, bạn có thể tải về Bitcoin.com Wallet

23
00:01:09,460 --> 00:01:12,120
cho các thiết bị iOS hoặc Android.

24
00:01:12,980 --> 00:01:15,440
Cuối cùng, chọn đồng tiền được quy định tại nơi sử dụng

25
00:01:16,300 --> 00:01:17,300
Hoàn tất!

26
00:01:17,560 --> 00:01:18,840
Bạn đã sẵn sàng các thiết lập để sử dụng.

27
00:01:19,165 --> 00:01:21,555
Nhập giá trị cần thiết để tạo mã QR.

28
00:01:22,075 --> 00:01:25,025
Khách hàng của bạn chỉ cần quét và xác nhận thanh toán

29
00:01:25,025 --> 00:01:27,115
và nó sẽ được gửi trực tiếp đến ví của bạn.

30
00:01:28,040 --> 00:01:30,480
Vì ứng dụng được bảo vệ bằng mã pin,

31
00:01:30,480 --> 00:01:33,220
chỉ có Mary có thể quyết định nơi mà các quỹ tiền được gửi.

32
00:01:33,795 --> 00:01:36,945
Đơn giản, an toàn và nhanh hơn Lightning.

33
00:01:37,755 --> 00:01:39,700
Đăng ký Bitcoin Cash ngay
