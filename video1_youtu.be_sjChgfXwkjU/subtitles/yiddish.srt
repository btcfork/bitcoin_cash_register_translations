﻿1
00:00:00,100 --> 00:00:03,360
 מרים איז אַ קאַווע קראָם באַזיצער און באַשלאָסן אַז זי וויל צו אָננעמען

2
00:00:03,360 --> 00:00:05,875
 ביטקאָין קאַש פּיימאַנץ צו צוציען נייַ קאַסטאַמערז.

3
00:00:05,880 --> 00:00:11,200
 זי דאַונלאָודיד די Bitcoin.com בייַטל און סטאַרטעד אַקסעפּטינג ביטקאָין קאַש גלייך.

4
00:00:11,360 --> 00:00:14,180
 "מרים איז יוזשאַוואַלי אין די קראָם, אָבער יז

5
00:00:14,180 --> 00:00:17,475
 זי דאַרף אוועקגייען און עס פירן דורך אירע צוויי אַסיסטאַנץ,

6
00:00:17,475 --> 00:00:18,685
 פעטרוס און שרה.

7
00:00:19,060 --> 00:00:21,620
 מרים געוואלט אַן גרינג וועג פֿאַר פעטרוס און שרה

8
00:00:21,620 --> 00:00:25,120
 פאָרזעצן אַקסעפּטינג ביטקאָין קאַש פּיימאַנץ בשעת זי איז געווען אַוועק פון די קראָם.

9
00:00:25,615 --> 00:00:27,975
 אַרייַן די ביטקאָין קאַש רעגיסטרירן אַפּ.

10
00:00:28,415 --> 00:00:31,495
 מרים קענען ינסטאַלירן די ביטקאָין קאַש רעגיסטרירן אַפּ

11
00:00:31,500 --> 00:00:34,620
 גלייַך אויף די טעלעפאָן פון פעטרוס און שרה, אָדער אויף די קראָם טאַבלעט,

12
00:00:34,620 --> 00:00:37,560
 און האָבן פּיימאַנץ צו די קראָם געשיקט גלייַך צו איר אייגן בייַטל.

13
00:00:37,720 --> 00:00:40,480
 איצט מרים האט שלום פון גייַסט אַז זי קען אָננעמען

14
00:00:40,480 --> 00:00:43,740
 שנעל, ביליק און פאַרלאָזלעך ביטקאָין קאַש פּיימאַנץ,

15
00:00:43,740 --> 00:00:45,740
 אפילו ווען זי איז נישט אין די קראָם.

16
00:00:45,740 --> 00:00:47,040
 אַזוי ווי האט מרים טאָן דאָס?

17
00:00:47,575 --> 00:00:50,675
 צו באַקומען סטאַרטעד, פשוט גיין צו די אַפּ סטאָר אָדער פּלייַ קראָם

18
00:00:50,680 --> 00:00:52,860
 און אראפקאפיע די ביטקאָין קאַש רעגיסטרירן אַפּ.

19
00:00:53,720 --> 00:00:55,320
 איצט לאָזן אונדז שטעלן זיך.

20
00:00:55,880 --> 00:00:59,980
 נאָך באַשטעטיקן דיין שפּילקע קאָד, אָנהייבן מיט ינפּוט די נאָמען פון דיין קראָם.

21
00:01:00,500 --> 00:01:04,600
 דריקט אויף דעסטיניישאַן אַדרעס און יבערקוקן אָדער פּאַפּ דיין ביטקאָין קאַש ריסיווינג אַדרעס

22
00:01:05,760 --> 00:01:09,460
 אויב איר טאָן ניט האָבן אַ ביטקאָין קאַש בייַטל, איר קענען אראפקאפיע די Bitcoin.com בייַטל

23
00:01:09,460 --> 00:01:12,120
 פֿאַר יאָס אָדער אַנדרויד דעוויסעס.

24
00:01:12,980 --> 00:01:15,440
 צום סוף, סעלעקטירן דיין לאקאלע קראַנטקייַט.

25
00:01:16,300 --> 00:01:17,300
 דאס איז עס!

26
00:01:17,560 --> 00:01:18,840
 איר זענט אַלע באַשטימט צו גיין.

27
00:01:19,165 --> 00:01:21,555
 אַרייַן די סומע צו דזשענערייט אַ QR קאָד.

28
00:01:22,075 --> 00:01:25,025
 דיין קאַסטאַמערז נאָר דאַרפֿן צו יבערקוקן און באַשטעטיקן צאָלונג

29
00:01:25,025 --> 00:01:27,115
 און עס וועט זיין געשיקט גלייַך צו דיין בייַטל.

30
00:01:28,040 --> 00:01:30,480
 זינט די אַפּ איז פּראָטעקטעד דורך אַ שטיפט-קאָד,

31
00:01:30,480 --> 00:01:33,220
 בלויז מרים קענען באַשליסן וואו די געלט זענען געשיקט.

32
00:01:33,795 --> 00:01:36,945
 פּשוט, זיכער און פאַסטער ווי בליץ.

33
00:01:37,755 --> 00:01:39,700
 ביטקאָין קאַש רעגיסטרירן

