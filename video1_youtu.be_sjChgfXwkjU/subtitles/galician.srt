﻿1
00:00:00,100 --> 00:00:03,360
 Mary é propietaria dunha cafetería e decidiu que quere aceptar

2
00:00:03,360 --> 00:00:05,875
 Pagamentos en efectivo de Bitcoin para atraer novos clientes.

3
00:00:05,880 --> 00:00:11,200
 Ela descargou a carteira Bitcoin.com e comezou a aceptar Bitcoin Cash de inmediato.

4
00:00:11,360 --> 00:00:14,180
 "María adoita estar na tenda, pero ás veces

5
00:00:14,180 --> 00:00:17,475
 ela necesita saír e deixala dirixida polos seus dous axudantes,

6
00:00:17,475 --> 00:00:18,685
 Peter e Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary quería un camiño doado para Peter e Sarah

8
00:00:21,620 --> 00:00:25,120
 para continuar aceptando pagos de Bitcoin Cash mentres estaba lonxe da tenda.

9
00:00:25,615 --> 00:00:27,975
 Acceda á aplicación de rexistro de caixa Bitcoin.

10
00:00:28,415 --> 00:00:31,495
 Mary pode instalar a aplicación Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
 directamente no teléfono de Peter e Sarah ou na tablet da tenda,

12
00:00:34,620 --> 00:00:37,560
 e ter enviados directamente á tenda á súa propia carteira.

13
00:00:37,720 --> 00:00:40,480
 Agora María ten tranquilidade de que pode aceptar

14
00:00:40,480 --> 00:00:43,740
 Pagamentos en Bitcoin rápido, barato e fiable,

15
00:00:43,740 --> 00:00:45,740
 mesmo cando non está na tenda.

16
00:00:45,740 --> 00:00:47,040
 Entón, como fixo Mary?

17
00:00:47,575 --> 00:00:50,675
 Para comezar, simplemente vaia á App Store ou Play Store

18
00:00:50,680 --> 00:00:52,860
 e descarga a aplicación Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
 Agora, imos facer que se configure.

20
00:00:55,880 --> 00:00:59,980
 Despois de establecer o seu código pin, comeza por introducir o nome da túa tenda.

21
00:01:00,500 --> 00:01:04,600
 Faga clic en Enderezo de destino e Escanear ou pegue o seu enderezo de recepción de diñeiro Bitcoin

22
00:01:05,760 --> 00:01:09,460
 Se non tes unha carteira Bitcoin Cash, podes descargar a carteira Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
 para dispositivos iOS ou Android.

24
00:01:12,980 --> 00:01:15,440
 Finalmente, selecciona a súa moeda local.

25
00:01:16,300 --> 00:01:17,300
 Iso si!

26
00:01:17,560 --> 00:01:18,840
 Está todo listo para ir.

27
00:01:19,165 --> 00:01:21,555
 Insira o importe para xerar un código QR.

28
00:01:22,075 --> 00:01:25,025
 Os seus clientes só precisan dixitalizar e confirmar o pago

29
00:01:25,025 --> 00:01:27,115
 e enviarase directamente á túa carteira.

30
00:01:28,040 --> 00:01:30,480
 Dado que a aplicación está protexida por un código pin,

31
00:01:30,480 --> 00:01:33,220
 só María pode decidir onde se envían os fondos.

32
00:01:33,795 --> 00:01:36,945
 Sinxelo, seguro e máis rápido que o raio.

33
00:01:37,755 --> 00:01:39,700
 Rexistro de caixa Bitcoin

