﻿1
00:00:00,100 --> 00:00:03,360
 Mary on kahvilan omistaja ja päätti haluavansa hyväksyä sen

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash -maksut uusien asiakkaiden houkuttelemiseksi.

3
00:00:05,880 --> 00:00:11,200
 Hän latasi Bitcoin.com-lompakon ja alkoi heti hyväksyä Bitcoin Cash.

4
00:00:11,360 --> 00:00:14,180
 Mary on yleensä kaupassa, mutta joskus

5
00:00:14,180 --> 00:00:17,475
 hänen täytyy astua pois ja saada se hoitamaan kaksi avustajaaan,

6
00:00:17,475 --> 00:00:18,685
 Pietari ja Saara.

7
00:00:19,060 --> 00:00:21,620
 Mary halusi helpon tavan Pietarille ja Saaralle

8
00:00:21,620 --> 00:00:25,120
 jatkaa Bitcoin Cash -maksujen hyväksymistä hänen ollessa poissa kaupasta.

9
00:00:25,615 --> 00:00:27,975
 Anna Bitcoin-kassakirjasovellus.

10
00:00:28,415 --> 00:00:31,495
 Mary voi asentaa Bitcoin Cash Register -sovelluksen

11
00:00:31,500 --> 00:00:34,620
 suoraan Pietarin ja Saaran puhelimeen tai myymälän tablet-laitteeseen,

12
00:00:34,620 --> 00:00:37,560
 ja lähettää maksut myymälään suoraan omaan lompakkoonsa.

13
00:00:37,720 --> 00:00:40,480
 Nyt Marialla on mielenrauha, jonka hän voi hyväksyä

14
00:00:40,480 --> 00:00:43,740
 nopeat, halvat ja luotettavat Bitcoin Cash -maksut,

15
00:00:43,740 --> 00:00:45,740
 jopa silloin, kun hän ei ole kaupassa.

16
00:00:45,740 --> 00:00:47,040
 Joten miten Mary teki sen?

17
00:00:47,575 --> 00:00:50,675
 Aloita siirtymällä App Store- tai Play-kauppaan

18
00:00:50,680 --> 00:00:52,860
 ja lataa Bitcoin Cash Register -sovellus.

19
00:00:53,720 --> 00:00:55,320
 Nyt saadaan sinun perustamaan.

20
00:00:55,880 --> 00:00:59,980
 Kun olet asettanut PIN-koodisi, aloita antamalla myymälän nimi.

21
00:01:00,500 --> 00:01:04,600
 Napsauta Kohdeosoite ja skannaa tai liitä Bitcoin Cash Reading -osoitteesi

22
00:01:05,760 --> 00:01:09,460
 Jos sinulla ei ole Bitcoin Cash-lompakkoa, voit ladata Bitcoin.com-lompakon

23
00:01:09,460 --> 00:01:12,120
 iOS- tai Android-laitteille.

24
00:01:12,980 --> 00:01:15,440
 Valitse lopuksi paikallinen valuutta.

25
00:01:16,300 --> 00:01:17,300
 Se siitä!

26
00:01:17,560 --> 00:01:18,840
 Olet valmis menemään.

27
00:01:19,165 --> 00:01:21,555
 Kirjoita summa QR-koodin luomiseksi.

28
00:01:22,075 --> 00:01:25,025
 Asiakkaidesi tarvitsee vain tarkistaa ja vahvistaa maksu

29
00:01:25,025 --> 00:01:27,115
 ja se lähetetään suoraan lompakkoosi.

30
00:01:28,040 --> 00:01:30,480
 Koska sovellus on suojattu pin-koodilla,

31
00:01:30,480 --> 00:01:33,220
 Vain Mary voi päättää, mihin varat lähetetään.

32
00:01:33,795 --> 00:01:36,945
 Yksinkertainen, turvallinen ja nopeampi kuin salama.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin-kassakone

