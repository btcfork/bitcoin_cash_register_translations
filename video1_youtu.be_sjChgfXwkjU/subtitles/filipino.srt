﻿1
00:00:00,100 --> 00:00:03,360
Si Mary ay nagmamay-ari ng tindahan ng mga kape at nagdesisyon
siyang tumanggap ng

2
00:00:03,360 --> 00:00:05,875
Bitcoin Cash bilang bayad upang makaenganyo ng bagong mamimili.

3
00:00:05,880 --> 00:00:11,200
Siya ay nagdownload ng Bitcoin.com wallet at kaagarang nagsimulang tumanggap ng Bitcoin Cash bilang bayad.

4
00:00:11,360 --> 00:00:14,180
Si Mary ay laging nasa kanyang tindahan, ngunit minsan

5
00:00:14,180 --> 00:00:17,475
ay kailangan niyang lumabas at
ipaubaya ito sa dalawang katulong niya,

6
00:00:17,475 --> 00:00:18,685
sina Peter at Sarah.

7
00:00:19,060 --> 00:00:21,620
Gusto ni Mary ng isang madaling paraan para kay Peter at Sarah

8
00:00:21,620 --> 00:00:25,120
na ipagpatuloy ang pagtanggap ng Bitcoin Cash bilang bayad habang malayo siya sa kanyang tindahan.

9
00:00:25,615 --> 00:00:27,975
Ipinapakilala namin sa inyo ang Bitcoin Cash Register App.

10
00:00:28,415 --> 00:00:31,495
Si Mary ay maaring mag-install ng Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
direkta sa cellphone nina Peter at Sarah, o sa tablet na nasa tindahan,

12
00:00:34,620 --> 00:00:37,560
at ang lahat ng bayad mula sa kanyang tindahan ay direktang mapupunta sa kanyang wallet.

13
00:00:37,720 --> 00:00:40,480
Ngayon ay kampante na si Mary na makatanggap ng

14
00:00:40,480 --> 00:00:43,740
mabilis, mura at maasahang Bitcoin Cash bilang bayad,

15
00:00:43,740 --> 00:00:45,740
kahit wala siya sa kanyang tindahan.

16
00:00:45,740 --> 00:00:47,040
Paano nga ba ito ginawa ni Mary?

17
00:00:47,575 --> 00:00:50,675
Para makapag-umpisa, pumunta lamang sa App Store o Play Store

18
00:00:50,680 --> 00:00:52,860
at i-download ang Bitcoin Cash Register App.

19
00:00:53,720 --> 00:00:55,320
Ngayon, magsimula na tayo.

20
00:00:55,880 --> 00:00:59,980
Pagkatapos mong maglagay ng pin-code, ilagay muna ang pangalan ng iyong tindahan.

21
00:01:00,500 --> 00:01:04,600
I-click ang Destination Address at i-scan o i-paste ang iyong Bitcoin Cash Receiving address.

22
00:01:05,760 --> 00:01:09,460
Kapag wala kapang Bitcoin Cash wallet, maaring mong i-download ang Bitcoin.com Wallet

23
00:01:09,460 --> 00:01:12,120
para sa mga iOS o Android devices.

24
00:01:12,980 --> 00:01:15,440
At ang panghuli, ilagay ang ating lokal na kurensiya.

25
00:01:16,300 --> 00:01:17,300
Iyan lamang!

26
00:01:17,560 --> 00:01:18,840
Ikaw ay handang-handa na.

27
00:01:19,165 --> 00:01:21,555
Ilagay lamang ang halaga upang makagawa ng QR code.

28
00:01:22,075 --> 00:01:25,025
At ang iyong mamimili ay kailangan lamang i-scan ito at kumpirmahin ang bayad

29
00:01:25,025 --> 00:01:27,115
at ito ay direktang mapupunta sa iyong wallet.

30
00:01:28,040 --> 00:01:30,480
At dahil ang app ay protektado ng pin-code,

31
00:01:30,480 --> 00:01:33,220
si Mary lamang ang makakapagdesiyon kung saan mapupunta ang bayad.

32
00:01:33,795 --> 00:01:36,945
Simple, Segurado, at Mabilis pa kaysa sa Kilat.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register
