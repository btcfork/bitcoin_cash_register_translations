﻿1
00:00:00,100 --> 00:00:03,360
 Mary estas posedantino de kafejo kaj decidis ke ŝi volas akcepti

2
00:00:03,360 --> 00:00:05,875
 Pagoj pri Bitcoin Cash por allogi novajn klientojn.

3
00:00:05,880 --> 00:00:11,200
 Ŝi elŝutis la monujo Bitcoin.com kaj komencis akcepti Bitcoin Cash tuj.

4
00:00:11,360 --> 00:00:14,180
 Mary kutime estas ĉe la butiko, sed foje

5
00:00:14,180 --> 00:00:17,475
 ŝi bezonas forpaŝi kaj ĝin administri de siaj du helpantoj,

6
00:00:17,475 --> 00:00:18,685
 Petro kaj Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary deziris facilan vojon por Petro kaj Sarah

8
00:00:21,620 --> 00:00:25,120
 daŭrigi akcepti pagojn de Bitcoin Cash dum ŝi estis for de la vendejo.

9
00:00:25,615 --> 00:00:27,975
 Eniru la Bitcoin Cash Register App.

10
00:00:28,415 --> 00:00:31,495
 Mary povas instali la Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
 rekte sur la telefono de Petro kaj Sarah, aŭ sur la butika tablojdo,

12
00:00:34,620 --> 00:00:37,560
 kaj havas pagojn al la vendejo sendita rekte al ŝia propra monujo.

13
00:00:37,720 --> 00:00:40,480
 Nun Maria havas trankvilon, ke ŝi povas akcepti

14
00:00:40,480 --> 00:00:43,740
 rapidaj, malmultekostaj kaj fidindaj Bitcoin Cash-pagoj,

15
00:00:43,740 --> 00:00:45,740
 eĉ kiam ŝi ne estas en la butiko.

16
00:00:45,740 --> 00:00:47,040
 Do kiel faris ĝin Maria?

17
00:00:47,575 --> 00:00:50,675
 Por komenci, simple iru al la App Store aŭ Play Store

18
00:00:50,680 --> 00:00:52,860
 kaj elŝuti la Bitcoin Cash Register App.

19
00:00:53,720 --> 00:00:55,320
 Nun, ni aranĝu vin agordi.

20
00:00:55,880 --> 00:00:59,980
 Post agordi vian pinglokodon, komencu per enmeto de la nomo de via vendejo.

21
00:01:00,500 --> 00:01:04,600
 Alklaku Helpa Adreso kaj Skanu aŭ Algluu vian Bitcoin Cash Ricevantan adreson

22
00:01:05,760 --> 00:01:09,460
 Se vi ne havas monon de Bitcoin Cash, vi povas elŝuti la Bitcoin Wallet

23
00:01:09,460 --> 00:01:12,120
 por iOS aŭ Android-aparatoj.

24
00:01:12,980 --> 00:01:15,440
 Fine elektu vian Lokan Moneron.

25
00:01:16,300 --> 00:01:17,300
 Jen ĝi!

26
00:01:17,560 --> 00:01:18,840
 Vi pretas iri.

27
00:01:19,165 --> 00:01:21,555
 Enmetu la sumon por generi QR-kodon.

28
00:01:22,075 --> 00:01:25,025
 Viaj klientoj nur bezonas skani kaj konfirmi pagon

29
00:01:25,025 --> 00:01:27,115
 kaj ĝi estos sendita rekte al via monujo.

30
00:01:28,040 --> 00:01:30,480
 Ĉar la app estas protektita per pinĉilo,

31
00:01:30,480 --> 00:01:33,220
 nur Maria povas decidi kien estas sendita la financo.

32
00:01:33,795 --> 00:01:36,945
 Simpla, Sekura, kaj Pli Rapida ol Fulmo.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin Cash Register

