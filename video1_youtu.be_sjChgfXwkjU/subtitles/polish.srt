﻿1
00:00:00,100 --> 00:00:03,360
Mary jest właścicielkę kawiarni i zdecydowała, że chce przyjmować

2
00:00:03,360 --> 00:00:05,875
Płatności kryptowalutą Bitcoin Cash, aby zachęcić nowych klientów.

3
00:00:05,880 --> 00:00:11,200
Pobrała portfel Bitcoin.com i od razu zaczęła akceptować Bitcoin Cash.

4
00:00:11,360 --> 00:00:14,180
Mary zwykle jest w kawiarni, ale czasami

5
00:00:14,180 --> 00:00:17,475
pod jej nieobecność, kawiarnię prowadzi jej dwóch asystentów,

6
00:00:17,475 --> 00:00:18,685
Piotr i Sara.

7
00:00:19,060 --> 00:00:21,620
Mary chciała znaleźć łatwy sposób, aby Piotr i Sara

8
00:00:21,620 --> 00:00:25,120
mogli kontynuować przyjmowanie płatności Bitcoin Cash podczas jej nieobecności.

9
00:00:25,615 --> 00:00:27,975
Poznaj aplikację Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
Mary może zainstalować aplikację Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
bezpośrednio na telefonie Piotra i Sary lub na tablecie sklepowym,

12
00:00:34,620 --> 00:00:37,560
która wysyła płatności dokonywane w kawiarni bezpośrednio do jej portfela.

13
00:00:37,720 --> 00:00:40,480
Teraz Mary może być spokojna, bo ma system, który akceptuje

14
00:00:40,480 --> 00:00:43,740
szybkie, tanie i niezawodne płatności Bitcoin Cash,

15
00:00:43,740 --> 00:00:45,740
nawet gdy nie ma jej w kawiarni.

16
00:00:45,740 --> 00:00:47,040
Jak coś takiego zrobić?

17
00:00:47,575 --> 00:00:50,675
Aby rozpocząć, po prostu przejdź do sklepu App Store lub Google Play

18
00:00:50,680 --> 00:00:52,860
i pobierz aplikację Bitcoin Cash Register

19
00:00:53,720 --> 00:00:55,320
Teraz dokonaj twoich ustawień.

20
00:00:55,880 --> 00:00:59,980
Po ustawieniu kodu PIN zacznij od wprowadzenia nazwy swojego sklepu.

21
00:01:00,500 --> 00:01:04,600
Kliknij na Adres docelowy i zeskanuj lub wklej twój adres Bitcoin Cash

22
00:01:05,760 --> 00:01:09,460
Jeśli nie masz portfela Bitcoin Cash, możesz pobrać portfel Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
na urządzenia z systemem iOS lub Android.

24
00:01:12,980 --> 00:01:15,440
Na koniec wybierz twoją walutę.

25
00:01:16,300 --> 00:01:17,300
I już!

26
00:01:17,560 --> 00:01:18,840
Wszystko gotowe.

27
00:01:19,165 --> 00:01:21,555
Wprowadź kwotę, aby wygenerować kod QR.

28
00:01:22,075 --> 00:01:25,025
Twoi klienci muszą tylko zeskanować i potwierdzić płatność

29
00:01:25,025 --> 00:01:27,115
i zostanie ona wysłana bezpośrednio do Twojego portfela.

30
00:01:28,040 --> 00:01:30,480
Ponieważ aplikacja jest chroniona kodem PIN,

31
00:01:30,480 --> 00:01:33,220
tylko ty możesz decydować o tym, gdzie pieniądze zostaną wysłane.

32
00:01:33,795 --> 00:01:36,945
Prosty, bezpieczny i szybszy niż błyskawica.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register

