﻿1
00:00:00,100 --> 00:00:03,360
Marie est propriétaire d'un café
et a décidé qu'elle veut accepter

2
00:00:03,360 --> 00:00:05,875
les paiements en Bitcoin Cash
pour attirer de nouveaux clients.

3
00:00:05,880 --> 00:00:11,200
Elle a téléchargé le portefeuille Bitcoin.com
et a immédiatement commencé à accepter le Bitcoin Cash.

4
00:00:11,360 --> 00:00:14,180
Marie est généralement à la boutique, mais parfois

5
00:00:14,180 --> 00:00:17,475
elle a besoin de s'éloigner
et de la laisser à ses deux assistants,

6
00:00:17,475 --> 00:00:18,685
Peter et Sarah.

7
00:00:19,060 --> 00:00:21,620
Marie voulait un moyen simple pour Peter et Sarah

8
00:00:21,620 --> 00:00:25,120
de continuer à accepter les paiements en Bitcoin Cash
pendant qu'elle était absente de la boutique.

9
00:00:25,615 --> 00:00:27,975
C'est là que l'application Bitcoin Cash Register intervient.

10
00:00:28,415 --> 00:00:31,495
Marie peut installer l'application Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
directement sur le téléphone de Peter et Sarah,
ou sur la tablette de la boutique,

12
00:00:34,620 --> 00:00:37,560
et avoir les paiements de la boutique envoyés directement
vers son portefeuille.

13
00:00:37,720 --> 00:00:40,480
A présent, Marie a l'esprit tranquille et peut accepter

14
00:00:40,480 --> 00:00:43,740
des paiements en Bitcoin Cash rapides, peu chers, et fiables,

15
00:00:43,740 --> 00:00:45,740
même quand elle n'est pas à la boutique.

16
00:00:45,740 --> 00:00:47,040
Alors, comment Marie a-t-elle fait?

17
00:00:47,575 --> 00:00:50,675
Pour commencer, rendez-vous simplement dans l'App Store ou sur le Play store

18
00:00:50,680 --> 00:00:52,860
et téléchargez l'application Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
A présent, passons à la configuration.

20
00:00:55,880 --> 00:00:59,980
Après avoir défini votre code PIN, commencez par entrer le nom de votre boutique.

21
00:01:00,500 --> 00:01:04,600
Cliquez sur Adresse de Destination et scannez ou collez
votre Adresse de Réception Bitcoin Cash

22
00:01:05,760 --> 00:01:09,460
Si vous n'avez pas de portefeuille Bitcoin Cash,
vous pouvez télécharger le portefeuille Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
pour iOS ou Android.

24
00:01:12,980 --> 00:01:15,440
Enfin, choisissez votre Monnaie Locale.

25
00:01:16,300 --> 00:01:17,300
Ça y est!

26
00:01:17,560 --> 00:01:18,840
Vous êtes prêt!

27
00:01:19,165 --> 00:01:21,555
Entrez le montant pour générer un code QR.

28
00:01:22,075 --> 00:01:25,025
Vos clients doivent juste scanner et confirmer le paiement

29
00:01:25,025 --> 00:01:27,115
et celui-ci sera directement transféré vers votre portefeuille.

30
00:01:28,040 --> 00:01:30,480
Comme l'application est protégée par un code PIN,

31
00:01:30,480 --> 00:01:33,220
seulement Marie peut décider où les fonds seront envoyés.

32
00:01:33,795 --> 00:01:36,945
Simple, sûr, et plus rapide que l'éclair.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register
