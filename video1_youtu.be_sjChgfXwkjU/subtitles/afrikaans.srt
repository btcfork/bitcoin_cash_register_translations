﻿1
00:00:00,100 --> 00:00:03,360
 Mary is 'n koffiewinkel-eienaar en besluit dat sy dit wil aanvaar

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash-betalings om nuwe kliënte te lok.

3
00:00:05,880 --> 00:00:11,200
 Sy laai die Bitcoin.com-beursie af en begin dadelik Bitcoin Cash aanvaar.

4
00:00:11,360 --> 00:00:14,180
 'Mary is gewoonlik by die winkel, maar soms

5
00:00:14,180 --> 00:00:17,475
 sy moet wegstap en laat dit deur haar twee assistente bestuur word,

6
00:00:17,475 --> 00:00:18,685
 Peter en Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary wou 'n maklike manier vir Peter en Sarah hê

8
00:00:21,620 --> 00:00:25,120
 om voort te gaan met die betaling van Bitcoin Cash-betalings terwyl sy buite die winkel was.

9
00:00:25,615 --> 00:00:27,975
 Voer die Bitcoin-kasregister-app in.

10
00:00:28,415 --> 00:00:31,495
 Mary kan die Bitcoin Cash Register-app installeer

11
00:00:31,500 --> 00:00:34,620
 direk op Peter en Sarah se telefoon, of op die winkel-tablet,

12
00:00:34,620 --> 00:00:37,560
 en betalings na die winkel direk na haar eie beursie gestuur word.

13
00:00:37,720 --> 00:00:40,480
 Nou het Maria gemoedsrus wat sy kan aanvaar

14
00:00:40,480 --> 00:00:43,740
 vinnige, goedkoop en betroubare Bitcoin Cash-betalings,

15
00:00:43,740 --> 00:00:45,740
 selfs as sy nie by die winkel is nie.

16
00:00:45,740 --> 00:00:47,040
 So, hoe het Maria dit gedoen?

17
00:00:47,575 --> 00:00:50,675
 Gaan eenvoudig na die App Store of Play-winkel om aan die gang te kom

18
00:00:50,680 --> 00:00:52,860
 en laai die Bitcoin Cash Register-app af.

19
00:00:53,720 --> 00:00:55,320
 Laat ons u nou opstel.

20
00:00:55,880 --> 00:00:59,980
 Begin met die invoer van die naam van u winkel nadat u u PIN-kode ingestel het.

21
00:01:00,500 --> 00:01:04,600
 Klik op Bestemmingsadres en skandeer of plak u Bitcoin-kontantontvangeradres in

22
00:01:05,760 --> 00:01:09,460
 As u nie 'n Bitcoin Cash-beursie het nie, kan u die Bitcoin.com-beursie aflaai

23
00:01:09,460 --> 00:01:12,120
 vir iOS- of Android-toestelle.

24
00:01:12,980 --> 00:01:15,440
 Kies ten slotte u plaaslike geldeenheid.

25
00:01:16,300 --> 00:01:17,300
 Dis dit!

26
00:01:17,560 --> 00:01:18,840
 Julle is gereed om te gaan.

27
00:01:19,165 --> 00:01:21,555
 Voer die bedrag in om 'n QR-kode te genereer.

28
00:01:22,075 --> 00:01:25,025
 U kliënte hoef net die betaling te skandeer en te bevestig

29
00:01:25,025 --> 00:01:27,115
 en dit sal direk na u beursie gestuur word.

30
00:01:28,040 --> 00:01:30,480
 Aangesien die app deur 'n pinkode beskerm word,

31
00:01:30,480 --> 00:01:33,220
 slegs Mary kan besluit waarheen die fondse gestuur word.

32
00:01:33,795 --> 00:01:36,945
 Eenvoudig, veilig en vinniger as weerlig.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin-kontantregister

