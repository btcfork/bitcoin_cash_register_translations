﻿1
00:00:00,100 --> 00:00:03,360
 Mary ni mmiliki wa duka la kahawa na aliamua kuwa anataka kukubali

2
00:00:03,360 --> 00:00:05,875
 Malipo ya Fedha ya Bitcoin kuvutia wateja wapya.

3
00:00:05,880 --> 00:00:11,200
 Alipakua mkoba wa Bitcoin.com na akaanza kukubali Bitcoin Pesa mara moja.

4
00:00:11,360 --> 00:00:14,180
 "Kwa kawaida Mariamu huwa dukani, lakini wakati mwingine

5
00:00:14,180 --> 00:00:17,475
 anahitaji kuondoka na kuendeshwa na wasaidizi wake wawili,

6
00:00:17,475 --> 00:00:18,685
 Peter na Sara.

7
00:00:19,060 --> 00:00:21,620
 Mariamu alitaka njia rahisi kwa Peter na Sara

8
00:00:21,620 --> 00:00:25,120
 kuendelea kukubali malipo ya Pesa ya Bitcoin wakati yeye alikuwa mbali na duka.

9
00:00:25,615 --> 00:00:27,975
 Ingiza Programu ya Kusajili ya Fedha ya Bitcoin.

10
00:00:28,415 --> 00:00:31,495
 Mariamu anaweza kufunga Programu ya Kusajili ya Fedha ya Bitcoin

11
00:00:31,500 --> 00:00:34,620
 moja kwa moja kwenye simu ya Peter na Sarah, au kwenye kibao cha duka,

12
00:00:34,620 --> 00:00:37,560
 na malipo kwa duka linalotumwa moja kwa moja kwa mkoba wake mwenyewe.

13
00:00:37,720 --> 00:00:40,480
 Sasa Mariamu ana amani ya akili ambayo anaweza kukubali

14
00:00:40,480 --> 00:00:43,740
 malipo ya haraka na ya uhakika ya Fedha ya Fedha ya Bitcoin,

15
00:00:43,740 --> 00:00:45,740
 hata wakati hayuko dukani.

16
00:00:45,740 --> 00:00:47,040
 Kwa hivyo Maria alifanyaje?

17
00:00:47,575 --> 00:00:50,675
 Kuanza, nenda tu kwenye Duka la App au duka la Google

18
00:00:50,680 --> 00:00:52,860
 na upakue Programu ya Msajili wa Fedha ya Bitcoin.

19
00:00:53,720 --> 00:00:55,320
 Sasa, wacha tuweke.

20
00:00:55,880 --> 00:00:59,980
 Baada ya kuweka nambari yako ya pini, anza kwa kuingiza jina la duka lako.

21
00:01:00,500 --> 00:01:04,600
 Bonyeza Anwani ya Mwisho na Scan au Bandika anwani yako ya Upokeaji wa Fedha ya Bitcoin

22
00:01:05,760 --> 00:01:09,460
 Ikiwa hauna mkoba wa Fedha wa Bitcoin, unaweza kupakua mkoba wa Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
 kwa vifaa vya iOS au Android.

24
00:01:12,980 --> 00:01:15,440
 Mwishowe, chagua Fedha yako ya Mitaa.

25
00:01:16,300 --> 00:01:17,300
 Hiyo ni!

26
00:01:17,560 --> 00:01:18,840
 Ninyi nyote mko tayari kwenda.

27
00:01:19,165 --> 00:01:21,555
 Ingiza kiasi cha kutoa nambari ya QR.

28
00:01:22,075 --> 00:01:25,025
 Wateja wako wanahitaji tu kuchambua na kuthibitisha malipo

29
00:01:25,025 --> 00:01:27,115
 na itatumwa moja kwa moja kwa mkoba wako.

30
00:01:28,040 --> 00:01:30,480
 Kwa kuwa programu inalindwa na nambari ya pini,

31
00:01:30,480 --> 00:01:33,220
 ni Mariamu pekee anayeweza kuamua ni wapi pesa hutumwa.

32
00:01:33,795 --> 00:01:36,945
 Rahisi, Salama, na wepesi kuliko umeme.

33
00:01:37,755 --> 00:01:39,700
 Daftari la Fedha la Bitcoin

