﻿1
00:00:00,100 --> 00:00:03,360
 Mary is een coffeeshop eigenaar en besloot dat ze wil accepteren

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash-betalingen om nieuwe klanten aan te trekken.

3
00:00:05,880 --> 00:00:11,200
 Ze downloadde de Bitcoin.com-portemonnee en begon meteen Bitcoin Cash te accepteren.

4
00:00:11,360 --> 00:00:14,180
 Mary is meestal in de winkel, maar soms

5
00:00:14,180 --> 00:00:17,475
 ze moet weggaan en het laten runnen door haar twee assistenten,

6
00:00:17,475 --> 00:00:18,685
 Peter en Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary wilde een gemakkelijke manier voor Peter en Sarah

8
00:00:21,620 --> 00:00:25,120
 om Bitcoin Cash-betalingen te blijven accepteren terwijl ze weg was van de winkel.

9
00:00:25,615 --> 00:00:27,975
 Voer de Bitcoin-kassa-app in.

10
00:00:28,415 --> 00:00:31,495
 Mary kan de Bitcoin Cash Register-app installeren

11
00:00:31,500 --> 00:00:34,620
 rechtstreeks op de telefoon van Peter en Sarah, of op de winkeltablet,

12
00:00:34,620 --> 00:00:37,560
 en laat betalingen naar de winkel rechtstreeks naar haar eigen portemonnee sturen.

13
00:00:37,720 --> 00:00:40,480
 Nu heeft Mary gemoedsrust die ze kan accepteren

14
00:00:40,480 --> 00:00:43,740
 snelle, goedkope en betrouwbare Bitcoin Cash-betalingen,

15
00:00:43,740 --> 00:00:45,740
 zelfs als ze niet in de winkel is.

16
00:00:45,740 --> 00:00:47,040
 Dus hoe deed Mary het?

17
00:00:47,575 --> 00:00:50,675
 Ga naar de App Store of Play Store om te beginnen

18
00:00:50,680 --> 00:00:52,860
 en download de Bitcoin-kassa-app.

19
00:00:53,720 --> 00:00:55,320
 Laten we je nu instellen.

20
00:00:55,880 --> 00:00:59,980
 Nadat u uw pincode hebt ingesteld, begint u met het invoeren van de naam van uw winkel.

21
00:01:00,500 --> 00:01:04,600
 Klik op Bestemmingsadres en scan of plak uw Bitcoin Cash-ontvangstadres

22
00:01:05,760 --> 00:01:09,460
 Als u geen Bitcoin Cash-portemonnee hebt, kunt u de Bitcoin.com-portemonnee downloaden

23
00:01:09,460 --> 00:01:12,120
 voor iOS- of Android-apparaten.

24
00:01:12,980 --> 00:01:15,440
 Selecteer ten slotte uw lokale valuta.

25
00:01:16,300 --> 00:01:17,300
 Dat is het!

26
00:01:17,560 --> 00:01:18,840
 U bent helemaal klaar om te gaan.

27
00:01:19,165 --> 00:01:21,555
 Voer het bedrag in om een ​​QR-code te genereren.

28
00:01:22,075 --> 00:01:25,025
 Uw klanten hoeven alleen maar de betaling te scannen en te bevestigen

29
00:01:25,025 --> 00:01:27,115
 en het wordt rechtstreeks naar uw portemonnee verzonden.

30
00:01:28,040 --> 00:01:30,480
 Aangezien de app wordt beschermd door een pincode,

31
00:01:30,480 --> 00:01:33,220
 alleen Mary kan beslissen waar het geld naartoe wordt gestuurd.

32
00:01:33,795 --> 00:01:36,945
 Eenvoudig, veilig en sneller dan bliksem.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin-kassa

