﻿1
00:00:00,100 --> 00:00:03,360
 Mary este proprietarul unei cafenele și a decis că vrea să accepte

2
00:00:03,360 --> 00:00:05,875
 Plăți în numerar Bitcoin pentru a atrage clienți noi.

3
00:00:05,880 --> 00:00:11,200
 A descărcat portofelul Bitcoin.com și a început să accepte Bitcoin Cash imediat.

4
00:00:11,360 --> 00:00:14,180
 Mary este de obicei la magazin, dar uneori

5
00:00:14,180 --> 00:00:17,475
 trebuie să se îndepărteze și să fie condusă de cei doi asistenți,

6
00:00:17,475 --> 00:00:18,685
 Peter și Sarah.

7
00:00:19,060 --> 00:00:21,620
 Maria dorea o cale ușoară pentru Peter și Sarah

8
00:00:21,620 --> 00:00:25,120
 să continue să accepte plăți Bitcoin Cash în timp ce ea era departe de magazin.

9
00:00:25,615 --> 00:00:27,975
 Intrați în aplicația Bitcoin Cash Register.

10
00:00:28,415 --> 00:00:31,495
 Mary poate instala aplicația Bitcoin Cash Register

11
00:00:31,500 --> 00:00:34,620
 direct pe telefonul lui Peter și Sarah sau pe tableta magazinului,

12
00:00:34,620 --> 00:00:37,560
 și să plătească magazinului trimis direct în portofelul propriu.

13
00:00:37,720 --> 00:00:40,480
 Acum Maria are liniște sufletească pe care o poate accepta

14
00:00:40,480 --> 00:00:43,740
 plăți rapide, ieftine și de încredere în bani Bitcoin,

15
00:00:43,740 --> 00:00:45,740
 chiar și atunci când nu este la magazin.

16
00:00:45,740 --> 00:00:47,040
 Deci, cum a făcut-o Maria?

17
00:00:47,575 --> 00:00:50,675
 Pentru a începe, pur și simplu accesați App Store sau Play Store

18
00:00:50,680 --> 00:00:52,860
 și descărcați aplicația Bitcoin Cash Register.

19
00:00:53,720 --> 00:00:55,320
 Acum, hai să te pregătim.

20
00:00:55,880 --> 00:00:59,980
 După setarea codului dvs. pin, începeți prin introducerea numelui magazinului.

21
00:01:00,500 --> 00:01:04,600
 Faceți clic pe Adresa de destinație și Scanați sau Lipiți adresa dvs. de primire a banilor Bitcoin

22
00:01:05,760 --> 00:01:09,460
 Dacă nu aveți un portofel Bitcoin Cash, puteți descărca portofelul Bitcoin.com

23
00:01:09,460 --> 00:01:12,120
 pentru dispozitive iOS sau Android.

24
00:01:12,980 --> 00:01:15,440
 În cele din urmă, selectați moneda locală.

25
00:01:16,300 --> 00:01:17,300
 Asta e!

26
00:01:17,560 --> 00:01:18,840
 Sunteți gata să mergeți.

27
00:01:19,165 --> 00:01:21,555
 Introduceți suma pentru a genera un cod QR.

28
00:01:22,075 --> 00:01:25,025
 Clienții dvs. trebuie doar să scaneze și să confirme plata

29
00:01:25,025 --> 00:01:27,115
 și va fi trimis direct în portofel.

30
00:01:28,040 --> 00:01:30,480
 Întrucât aplicația este protejată de un cod pin,

31
00:01:30,480 --> 00:01:33,220
 numai Maria poate decide unde sunt trimise fondurile.

32
00:01:33,795 --> 00:01:36,945
 Simplu, sigur și mai rapid decât fulgerul.

33
00:01:37,755 --> 00:01:39,700
 Registrul de numerar Bitcoin

