﻿1
00:00:00,100 --> 00:00:03,360
 Mary er kaffebarseier og bestemte seg for at hun vil godta

2
00:00:03,360 --> 00:00:05,875
 Bitcoin Cash-betalinger for å tiltrekke seg nye kunder.

3
00:00:05,880 --> 00:00:11,200
 Hun lastet ned Bitcoin.com-lommeboken og kom i gang med å godta Bitcoin Cash med en gang.

4
00:00:11,360 --> 00:00:14,180
 Mary er vanligvis på butikken, men noen ganger

5
00:00:14,180 --> 00:00:17,475
 hun trenger å gå bort og få det kjørt av sine to assistenter,

6
00:00:17,475 --> 00:00:18,685
 Peter og Sarah.

7
00:00:19,060 --> 00:00:21,620
 Mary ønsket en enkel måte for Peter og Sarah

8
00:00:21,620 --> 00:00:25,120
 å fortsette å godta Bitcoin Cash-betalinger mens hun var borte fra butikken.

9
00:00:25,615 --> 00:00:27,975
 Gå inn på Bitcoin Cash Register-appen.

10
00:00:28,415 --> 00:00:31,495
 Mary kan installere Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
 direkte på Peter og Sarahs telefon, eller på butikkbrettet,

12
00:00:34,620 --> 00:00:37,560
 og har betaling til butikken sendt direkte til hennes egen lommebok.

13
00:00:37,720 --> 00:00:40,480
 Nå har Mary sinnsro som hun kan godta

14
00:00:40,480 --> 00:00:43,740
 raske, billige og pålitelige Bitcoin Cash-betalinger,

15
00:00:43,740 --> 00:00:45,740
 selv når hun ikke er i butikken.

16
00:00:45,740 --> 00:00:47,040
 Så hvordan gjorde Mary det?

17
00:00:47,575 --> 00:00:50,675
 For å komme i gang, gå ganske enkelt til App Store eller Play-butikken

18
00:00:50,680 --> 00:00:52,860
 og last ned Bitcoin Cash Register App.

19
00:00:53,720 --> 00:00:55,320
 La oss nå sette opp.

20
00:00:55,880 --> 00:00:59,980
 Etter å ha angitt PIN-koden, begynn med å skrive inn navnet på butikken din.

21
00:01:00,500 --> 00:01:04,600
 Klikk på Destinasjonsadresse og skann eller lim inn din Bitcoin Cash-mottakende adresse

22
00:01:05,760 --> 00:01:09,460
 Hvis du ikke har en Bitcoin Cash-lommebok, kan du laste ned Bitcoin.com-lommeboken

23
00:01:09,460 --> 00:01:12,120
 for iOS- eller Android-enheter.

24
00:01:12,980 --> 00:01:15,440
 Til slutt velger du din lokale valuta.

25
00:01:16,300 --> 00:01:17,300
 Det er det!

26
00:01:17,560 --> 00:01:18,840
 Du er klar til å gå.

27
00:01:19,165 --> 00:01:21,555
 Angi beløpet for å generere en QR-kode.

28
00:01:22,075 --> 00:01:25,025
 Kundene dine trenger bare å skanne og bekrefte betaling

29
00:01:25,025 --> 00:01:27,115
 og den vil bli sendt direkte til lommeboken din.

30
00:01:28,040 --> 00:01:30,480
 Siden appen er beskyttet av en pinkode,

31
00:01:30,480 --> 00:01:33,220
 bare Mary kan bestemme hvor midlene skal sendes.

32
00:01:33,795 --> 00:01:36,945
 Enkelt, sikkert og raskere enn lynet.

33
00:01:37,755 --> 00:01:39,700
 Bitcoin kassaapparat

