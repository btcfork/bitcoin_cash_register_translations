1
00:00:00,100 --> 00:00:03,360
Mary is a coffee shop owner and decided
she wants to accept

2
00:00:03,360 --> 00:00:05,875
Bitcoin Cash payments to attract new customers.

3
00:00:05,880 --> 00:00:11,200
She downloaded the Bitcoin.com wallet and got started accepting Bitcoin Cash right away.

4
00:00:11,360 --> 00:00:14,180
Mary is usually at the shop, but sometimes

5
00:00:14,180 --> 00:00:17,475
she needs to step away and
have it run by her two assistants,

6
00:00:17,475 --> 00:00:18,685
Peter and Sarah.

7
00:00:19,060 --> 00:00:21,620
Mary wanted an easy way for Peter and Sarah

8
00:00:21,620 --> 00:00:25,120
to continue accepting Bitcoin Cash payments while she was away from the store.

9
00:00:25,615 --> 00:00:27,975
Enter the Bitcoin Cash Register App.

10
00:00:28,415 --> 00:00:31,495
Mary can install the Bitcoin Cash Register App

11
00:00:31,500 --> 00:00:34,620
directly on Peter and Sarah’s phone, or on the store tablet,

12
00:00:34,620 --> 00:00:37,560
and have payments to the store sent directly to her own wallet.

13
00:00:37,720 --> 00:00:40,480
Now Mary has peace of mind that she can accept

14
00:00:40,480 --> 00:00:43,740
fast, cheap and reliable Bitcoin Cash payments,

15
00:00:43,740 --> 00:00:45,740
even when she isn't at the store.

16
00:00:45,740 --> 00:00:47,040
So how did Mary do it?

17
00:00:47,575 --> 00:00:50,675
To get started, simply go to the App Store or Play store

18
00:00:50,680 --> 00:00:52,860
and download the Bitcoin Cash Register App.

19
00:00:53,720 --> 00:00:55,320
Now, let's get you set up.

20
00:00:55,880 --> 00:00:59,980
After setting your pin code, begin by inputting the name of your store.

21
00:01:00,500 --> 00:01:04,600
Click Destination Address and Scan or Paste your Bitcoin Cash Receiving address

22
00:01:05,760 --> 00:01:09,460
If you don’t have a Bitcoin Cash wallet, you can download the Bitcoin.com Wallet

23
00:01:09,460 --> 00:01:12,120
for iOS or Android devices.

24
00:01:12,980 --> 00:01:15,440
Finally, select your Local Currency.

25
00:01:16,300 --> 00:01:17,300
That's it!

26
00:01:17,560 --> 00:01:18,840
You are all set to go.

27
00:01:19,165 --> 00:01:21,555
Enter the amount to generate a QR code.

28
00:01:22,075 --> 00:01:25,025
Your customers just need to scan and confirm payment

29
00:01:25,025 --> 00:01:27,115
and it will be sent directly to your wallet.

30
00:01:28,040 --> 00:01:30,480
Since the app is protected by a pin-code,

31
00:01:30,480 --> 00:01:33,220
only Mary can decide where the funds are sent.

32
00:01:33,795 --> 00:01:36,945
Simple, Secure, and Faster than Lightning.

33
00:01:37,755 --> 00:01:39,700
Bitcoin Cash Register

