#!/bin/bash
# First and only argument: name of .srt file to check sequence numbers
# and timecodes are compared against the english.srt file.
# if there is a difference, the script will display it, otherwise it will
# output "OK" if there is no difference.
#
# Note: In case the difference is only on the line with sequence number '1'
#       it is probably due to BOM difference.

if [ "x$1" = "x" ]
then
   echo "Usage: check_timestamps.sh <.srt file>"
   exit 1
fi
if [ ! -f "$1" ]
then
   echo "Error: Could not find input file $1"
   exit 1
fi

t=$(tempfile) || exit
trap "rm -f -- '$t'" EXIT
grep "^[0-9]" english.srt > "$t"

grep "^[0-9]" $1 | diff -q -- "$t" -
result=$?
if [ $result -ne 0 ]
then
   echo "Sequence numbers / timestamps differ from english.srt. Showing diff:"
   grep "^[0-9]" $1 | diff -- "$t" -
else
   echo "OK."
fi

rm -f -- "$t"
trap - EXIT
